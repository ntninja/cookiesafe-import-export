# Cookie Import/Export #

## Summary ##
Intended mainly for users previously using the
[FireFox CookieSafe](https://addons.mozilla.org/firefox/addon/cookiesafe-ff-4-compatible/)
extension, but may be useful for others as well. Previous exports done with the
CookieSafe add-on can be imported using this extension and are guaranteed to be
compatible.

**Unlike most other Firefox Cookie Import/Export extensions, this extension
allows you to migrate cookie permissions as well!**

## Interoperability ##
Cookie exports/imports do not use the usual Netscape cookie jar format but a
custom XML format that (to my knowledge) is not supported by any other program.
As there is no standard for cookie permissions a similar XML format is used for
these as well.

## Links ##

 * [Add-on listing on addons.mozilla.org](https://addons.mozilla.org/de/firefox/addon/cookie-importexport/)
 * [Version archive and Downloads](https://addons.mozilla.org/firefox/addon/cookie-importexport/versions/)
 * [Source code repository](https://gitlab.com/alexander255/cookiesafe-import-export/)