/*indent: 4, maxlen: 80, white: true */

const sdk = {
	self: require("sdk/self"),
	
	window: {
		utils: require("sdk/window/utils")
	}
};

const {Cc, Ci, Cu, Cr} = require("chrome");


module.exports = {
	/**
	 * Display a confirm message box with the given title and message
	 * string
	 * 
	 * @param {String} title
	 *        The title of the dialog which should be created
	 * @param {String} message
	 *        The message of the new dialog
	 * @return {Boolean} The result of the action (true: Yes, false: No)
	 */
	confirm: function(title, message) {
		var mozPromptService = Cc["@mozilla.org/embedcomp/prompt-service;1"]
		  .getService(Ci.nsIPromptService);
		
		return mozPromptService.confirm(window, title, message);
	},
	
	
	/**
	 * Display an error message cought by a try...catch block
	 *
	 * @param {Object} e
	 *        The error to display
	 */
	exception: function(e) {
		var message = e.toString() + "\n\n" + e.stack;
		
		// Display confirm message box
		return this.message(sdk.self.name, message);
	},
	
	
	/**
	 * Open a file selector dialog
	 * 
	 * The following options are supported:
	 *  - dirpath : `String` (Default: ``null``) 
	 *    The path of the directory that should be used as the initial directory shown by the file chooser
	 *  
	 *  - filename : `String` (Default: ``null``)
	 *    The initial file name that should be displayed by the file chooser
	 *  
	 *  - filters : `Array` (Default: ``["all"]``)
	 *    A list of file name matching filters that should be available to the user
	 *    
	 *    Each entry may either be string or a collection. If it is a string then the it will be interpreted as the name
	 *    of a built-in filter type (see
	 *    `MDN <https://developer.mozilla.org/en-US/docs/Mozilla/Tech/XPCOM/Reference/Interface/nsIFilePicker#Filter_constants>`_
	 *    for details), if it is a collection then the first item will be interpreted as filter title (as seen by the
	 *    user of the file chooser) and all remaining items will be interpreted as filename wildcard patterns to match
	 *    every file name against.
	 *  
	 *  - mode : `String` (Default: ``"open"``)
	 *    The file dialog mode
	 *    
	 *    Must have one of the following values: ``"open"``, ``"save"``, ``"getfolder"``, ``"openmultiple"``
	 *  
	 *  - title : `String` (Default: ``null``)
	 *    The window title to use for the dialog (default title will be used if none)
	 * 
	 * @return {String|Array|null}
	 */
	fileChooser: function(options) {
		var mozFileProtocolHandler = Cc["@mozilla.org/network/protocol;1?name=file"]
		  .getService(Ci.nsIFileProtocolHandler);
		
		// Process options
		var dirpath  = typeof(options.dirpath)  !== "undefined" ? options.dirpath  : null;
		var filename = typeof(options.filename) !== "undefined" ? options.filename : null;
		var filters  = typeof(options.filters)  !== "undefined" ? options.filters  : ["all"];
		var mode     = typeof(options.mode)     !== "undefined" ? options.mode     : "open";
		var title    = typeof(options.title)    !== "undefined" ? options.title    : null;
		
		// Create list of available builtin filters and dialog modes
		builtin_filters = {};
		builtin_modes   = {};
		for(var name in Ci.nsIFilePicker) {
			if(Ci.nsIFilePicker.hasOwnProperty(name)) {
				if(name.startsWith("filter")) {
					builtin_filters[name.toLowerCase().substr(6)] = Ci.nsIFilePicker[name];
				}
				if(name.startsWith("mode")) {
					builtin_modes[name.toLowerCase().substr(4)]   = Ci.nsIFilePicker[name];
				}
			}
		}
		
		// Create file picker instance
		var filepicker = Cc["@mozilla.org/filepicker;1"].createInstance(Ci.nsIFilePicker);
		filepicker.init(sdk.window.utils.getMostRecentBrowserWindow(), title, builtin_modes[mode.toLowerCase()]);
		
		// Add filters to file picker
		filters.forEach(function(filter) {
			if(typeof(filter) === "string") {
				filepicker.appendFilters(builtin_filters[filter.toLowerCase()]);
			} else {
				filepicker.appendFilter(filter[0], filter[1]);
			}
		});
		
		// Set file picker default directory
		try {
			var mozLocalFile = Cc["@mozilla.org/file/local;1"].createInstance(Ci.nsILocalFile);
			mozLocalFile.initWithPath(dirname);
			
			filepicker.displayDirectory = mozLocalFile;
		} catch(e) {}
		
		// Set file picker default file name
		if(filename) {
			filepicker.defaultString = filename;
		}
		
		// Show the file picker
		var result = filepicker.show();
		
		// Process the result
		if(result === Ci.nsIFilePicker.returnCancel) {
			return null;
		} else if(mode.toLowerCase().indexOf("multiple") > -1) {
			var filepaths = [];
			while(filepicker.files.hasMoreElements()) {
				filepaths.push(filepicker.files.getNext().QueryInterface(Ci.nsILocalFile).path);
			}
			return filepaths;
		} else {
			return filepicker.file.path;
		}
		
	},
	
	
	/**
	 * Display a normal message box with the given title and message
	 * string
	 * 
	 * @param {String} title
	 *        The title of the dialog which should be created
	 * @param {String} message
	 *        The message of the new dialog
	 */
	message: function(title, message) {
		var mozPromptService = Cc["@mozilla.org/embedcomp/prompt-service;1"]
		  .getService(Ci.nsIPromptService);
		
		return mozPromptService.alert(sdk.window.utils.getMostRecentBrowserWindow(), title, message);
	}
};