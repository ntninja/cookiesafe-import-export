/***************************************************************************
Copyright (C) 2007  Ron Beckman
Copyright (C) 2011  Alexander Schlarb

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to:

Free Software Foundation, Inc.
51 Franklin Street
Fifth Floor
Boston, MA  02110-1301
USA
***************************************************************************/

const {Cc, Ci, Cu, Cr} = require("chrome");
Cu.import("resource://gre/modules/NetUtil.jsm");



module.exports = {
	/**
	 * Create a new document object and return it
	 * 
	 * @param {String} parentname
	 *        The name of the parent node (eg: html)
	 * @return nsIDOMDocument
	 */
	createNewDocument: function(parentname) {
		var mozXMLParser = Cc["@mozilla.org/xmlextras/domparser;1"]
		  .getService(Ci.nsIDOMParser);
		
		return mozXMLParser.parseFromString(
		  "<"+parentname+"></"+parentname+">", "text/xml"
		);
	},
	
	
	createSafeTextNode: function(DOM, text) {
		text = new String(text);
		
		var result = [];
		for(var i = 0; i < text.length; i++) {
			// Strip control characters from input (they produce broken output)
			if(text.charCodeAt(i) >= 32) {
				result.push(text.charAt(i));
			}
		}
		return DOM.createTextNode(result.join(""));
	},
	
	
	/**
	 * Write data to a file (BLOCKING!)
	 * 
	 * @param {String} filepath
	 *        The location of the file
	 * @param {String} data
	 *        The data to write
	 * @return {Boolean} Success?
	 */
	write: function(filepath, data) {
		// Mozilla file protocol handler service
		var mozProtocolFile = Cc["@mozilla.org/network/protocol;1?name=file"]
		  .getService(Ci.nsIFileProtocolHandler);
		
		// Make sure the path is an absolute URL
		if(filepath.indexOf && filepath.indexOf("://") === -1) {
			filepath = "file://" + filepath;
		}
		
		// Open the filepath
		if(filepath instanceof Ci.nsIFile) {
			var fileobj = filepath;
		} else {
			var fileobj = mozProtocolFile.getFileFromURLSpec(filepath);
		}
		
		// Initialize the output stream:
		//  + Write, Create, Truncate
		//  + All: read, Group: read + write, User: read + write
		var filestream = Cc["@mozilla.org/network/file-output-stream;1"]
		  .createInstance(Ci.nsIFileOutputStream);
		filestream.init(fileobj, 0x02|0x08|0x20, 0664, 0);
		
		// Initialize the output stream converter
		var convstream = Cc["@mozilla.org/intl/converter-output-stream;1"]
		  .createInstance(Ci.nsIConverterOutputStream);
		convstream.init(filestream, "UTF-8", 0, 0);
		
		// Finally write data into the output file
		convstream.writeString(data);
		
		// Close output stream and stream converter
		convstream.close();
	},
	
	
	/**
	 * Read all data from a file
	 * 
	 * @param {String} filepath The location of the file
	 * @return {String} The data stored in the file
	 */
	read: function(filepath) {
		// Mozilla file protocol handler service
		var mozProtocolFile = Cc["@mozilla.org/network/protocol;1?name=file"]
		  .getService(Ci.nsIFileProtocolHandler);
		
		// Open the filepath
		var fileobj = mozProtocolFile.getFileFromURLSpec(filepath);
		
		// Mozilla file input stream
		var filestream = Cc["@mozilla.org/network/file-input-stream;1"]
		  .createInstance(Ci.nsIFileInputStream);
		// Initialize the input stream
		filestream.init(fileobj, -1, 0, 0);
		
		// Initialize input stream data converter
		var convstream = Cc["@mozilla.org/intl/converter-input-stream;1"]
		  .createInstance(Ci.nsIConverterInputStream)
		convstream.init(filestream, "UTF-8", 0, 0);
		
		// Finally read all the data from the file
		var data = "";
		var out  = {};
		do {
			var result = convstream.readString(0xffffffff, out);
			data += out.value;
		} while(result !== 0);
		
		// Close the input stream and stream converter
		convstream.close();
		
		return data;
	},
	
	
	//TODO: writeCookies: function(filepath) ASYNC!
	/**
	 * Write all cookies into a XML file
	 * 
	 * @param {String} filepath
	 *        The filepath of the file were the data is stored
	 * @return {Boolean} Success?
	 */
	writeCookiesSync: function(filepath) {
		// Mozilla cookie manager service
		var mozCookieManager = Cc["@mozilla.org/cookiemanager;1"]
		  .getService(Ci.nsICookieManager2);
		// Mozilla DOM serializer service
		var mozXMLSerializer = Cc["@mozilla.org/xmlextras/xmlserializer;1"]
		  .getService(Ci.nsIDOMSerializer);
		
		// Create DOM
		var DOM = this.createNewDocument("cookies");
		var DOMDocument = DOM.documentElement;
		
		// Loop through all elements stored in the permission manager
		var enumerator = mozCookieManager.enumerator;
		while((enumerator.hasMoreElements && enumerator.hasMoreElements())
		||    (enumerator.hasMore         && enumerator.hasMore())) {
			var cookie = enumerator.getNext();
			cookie.QueryInterface(Ci.nsICookie2);
			
			// Create DOM exception group
			var DOMCookie = DOM.createElement("cookie");
			
			// Create and write the DOM host field
			var DOMHost = DOM.createElement("host");
			DOMHost.appendChild(this.createSafeTextNode(DOM, cookie.host));
			
			// Create and write the DOM path field
			var DOMPath = DOM.createElement("path");
			DOMPath.appendChild(this.createSafeTextNode(DOM, cookie.path));
			
			// Create and write the DOM name field
			var DOMName = DOM.createElement("name");
			DOMName.appendChild(this.createSafeTextNode(DOM, cookie.name));
			
			// Create and write the DOM value field
			var DOMValue = DOM.createElement("value");
			DOMValue.appendChild(this.createSafeTextNode(DOM, cookie.value));
			
			// Create and write the DOM isSecure field
			var DOMSecure = DOM.createElement("secure");
			DOMSecure.appendChild(this.createSafeTextNode(DOM, cookie.isSecure));
			
			// Create and write the DOM isHttpOnly field
			var DOMHttp = DOM.createElement("http");
			if(typeof(cookie.isHttpOnly) != "undefined") {
				DOMHttp.appendChild(this.createSafeTextNode(DOM, cookie.isHttpOnly));
			} else {
				DOMHttp.appendChild(this.createSafeTextNode(DOM, "false"));
			}
			
			// Create and write the DOM isSecure field
			var DOMSession = DOM.createElement("session");
			DOMSession.appendChild(this.createSafeTextNode(DOM, cookie.isSession));
			
			// Create and write the DOM expiration field
			var DOMExpires = DOM.createElement("expires");
			DOMExpires.appendChild(this.createSafeTextNode(DOM, cookie.expires));
			
			// Add all DOM fields to the cookie information
			DOMCookie.appendChild(DOMHost);
			DOMCookie.appendChild(DOMPath);
			DOMCookie.appendChild(DOMName);
			DOMCookie.appendChild(DOMValue);
			DOMCookie.appendChild(DOMSecure);
			DOMCookie.appendChild(DOMHttp);
			DOMCookie.appendChild(DOMSession);
			DOMCookie.appendChild(DOMExpires);
			
			// Add exception to the DOM permissions list
			DOMDocument.appendChild(DOMCookie);
		}
		
		// Write the DOM tree to file
		return this.write(
		  filepath, mozXMLSerializer.serializeToString(DOM)
		);
	},
	
	
	//TODO: writeExceptions: function(filepath) ASYNC!
	/**
	 * Write all exceptions into a XML file
	 * 
	 * @param {String} filepath
	 *        The filepath of the file were the data is stored
	 * @return {Boolean} Success?
	 */
	writeExceptionsSync: function(filepath) {
		// Mozilla DOM serializer service
		var mozXMLSerializer = Cc["@mozilla.org/xmlextras/xmlserializer;1"]
		  .getService(Ci.nsIDOMSerializer);
		// Mozilla permission manager
		var mozPermManager = Cc["@mozilla.org/permissionmanager;1"]
		  .getService(Ci.nsIPermissionManager);
		
		// Create DOM
		var DOM = this.createNewDocument("permissions");
		var DOMDocument = DOM.documentElement;
		
		// Loop through all elements stored in the permission manager
		var enumerator = mozPermManager.enumerator;
		while((enumerator.hasMoreElements && enumerator.hasMoreElements())
		||    (enumerator.hasMore         && enumerator.hasMore())) {
			var perm = enumerator.getNext();
			perm.QueryInterface(Ci.nsIPermission);
			
			// We are only processing cookies
			if(perm.type != "cookie") {
				continue;
			}
			
			// Create DOM exception group
			var DOMException = DOM.createElement("exception");
			
			// Create DOM host field and write the hostname string
			var DOMHost = DOM.createElement("host");
			// Note: Firefox ~43 changed the way the hostname can be obtained
			//       https://bugzilla.mozilla.org/show_bug.cgi?id=1173523
			if(perm.principal && perm.principal.URI) {
				DOMHost.appendChild(this.createSafeTextNode(DOM, perm.principal.URI.prePath));
			} else {
				DOMHost.appendChild(this.createSafeTextNode(DOM, perm.host));
			}
			
			// Create DOM capability field and write the capability number
			var DOMCapability = DOM.createElement("capability");
			DOMCapability.appendChild(this.createSafeTextNode(DOM, perm.capability));
			
			// Add DOM host and DOM capability to the exception information
			DOMException.appendChild(DOMHost);
			DOMException.appendChild(DOMCapability);
			
			// Add exception to the DOM permissions list
			DOMDocument.appendChild(DOMException);
		}
		// Write the DOM tree to file
		return this.write(
		  filepath, mozXMLSerializer.serializeToString(DOM)
		);
	},
	
	
	/**
	 * Get a nsIChannel interface object from the URI given
	 * 
	 * @param {String|nsIFile|nsIURI|nsIChannel} URI
	 *        The URI from which the channel should be generated
	 * @return {nsIChannel|null}
	 */
	newChannelFromURI: function(URI) {
		// Mozilla network I/O service
		var mozIOService = Cc["@mozilla.org/network/io-service;1"]
		  .getService(Ci.nsIIOService);
		
		// Check if the URI is already a nsIChannel interface object
		if(URI instanceof Ci.nsIChannel) {
			return URI;
		} else {
			// Convert strings and nsIFile interface objects to nsIURIs
			if(!(URI instanceof Ci.nsIURI)) {
				if(URI instanceof Ci.nsIFile) {
					URI = mozIOService.newFileURI(URI);
				} else if(typeof(URI) === "string") {
					if(URI.indexOf("://") < 0) {
						URI = "file://" + URI;
					}
					
					URI = mozIOService.newURI(URI, null, null);
				} else {
					return null;
				}
			}
			
			return mozIOService.newChannelFromURI(URI);
		}
	},
	
	
	/**
	 * @see readCookies
	 * 
	 * WARNING: Do not use this function unless you have good reasons, it will
	 *          block the main thread until the whole reading and parsing
	 *          operation is completed
	 * 
	 * @param {String|nsIFile|nsIURI|nsIChannel|nsIInputStream} file
	 *        The filepath or input stream to read
	 * @return {Boolean} Success?
	 */
	readCookiesSync: function(file) {
		// Mozilla network I/O service
		var mozIOService = Cc["@mozilla.org/network/io-service;1"]
		  .getService(Ci.nsIIOService);
		// Mozilla XML DOM parser service
		var mozXMLDOMParser = Cc["@mozilla.org/xmlextras/domparser;1"]
		  .getService(Ci.nsIDOMParser);
		// Mozilla cookie manager service
		var mozCookieManager = Cc["@mozilla.org/cookiemanager;1"].
		  getService(Ci.nsICookieManager2);
		
		// Generate nsIInputStream from the file parameter
		var stream;
		if(file instanceof Ci.nsIInputStream) {
			// Data input stream (probably called from `readExceptions`)
			stream = file;
		} else {
			// Load the channel from the filepath given
			var channel = this.newChannelFromURI(file);
			if(!channel) {
				return false;
			}
			
			// Open a syncronous stream (BLOCKING!!!)
			stream = channel.open();
		}
		
		// Read data from nsIInputStream
		var data = NetUtil.readInputStreamToString(stream, stream.available());
		
		// Parse XML data into DOM
		var DOM = mozXMLDOMParser.parseFromString(data, "text/xml");
		var DOMDocument = DOM.documentElement;
		
		function trim(string) {
			return string.replace(/^\s*(\S*(\s+\S+)*)\s*$/, "$1");
		}
		
		// Santy check:
		//  Check if the document node is represented by the correct tag
		if(DOMDocument.tagName != "cookies") {
			return false;
		}
		
		for(var x=0; x < DOMDocument.childNodes.length; x++) {
			// Make sure the subnode is an element
			if([1, 9].indexOf(DOMDocument.childNodes[x].nodeType) < 0) {
				continue;
			}
			
			if(DOMDocument.childNodes[x].tagName == "cookie") {
				var DOMException = DOMDocument.childNodes[x];
				
				// Initialize host and capability data holders
				var host;
				var path;
				var name;
				var value;
				var secure;
				var httponly;
				var session;
				var expires;
				for(var y=0; y < DOMException.childNodes.length; y++) {
					// Make sure the subnode is an element
					if([1, 9].indexOf(DOMException.childNodes[y].nodeType) < 0) {
						continue;
					}
					
					if(DOMException.childNodes[y].tagName == "host") {
						host = trim(DOMException.childNodes[y].textContent);
						
						if(host == "scheme:file") {
							var uri =
							  mozIOService.newURI("file:///", null, null);
						} else if(host.indexOf("://") > -1) {
							var uri = mozIOService.newURI(host, null, null);
						} else {
							var uri =
							  mozIOService.newURI("http://" + host, null, null);
						}
					}
					
					if(DOMException.childNodes[y].tagName == "path") {
						path = trim(DOMException.childNodes[y].textContent);
					}
					
					if(DOMException.childNodes[y].tagName == "name") {
						name = trim(DOMException.childNodes[y].textContent);
					}
					
					if(DOMException.childNodes[y].tagName == "value") {
						value = trim(DOMException.childNodes[y].textContent);
					}
					
					if(DOMException.childNodes[y].tagName == "secure") {
						if(trim(DOMException.childNodes[y].textContent) == "false") {
							secure = false;
						} else {
							secure = true;
						}
					}
					
					if(DOMException.childNodes[y].tagName == "http") {
						if(trim(DOMException.childNodes[y].textContent) == "false") {
							httponly = false;
						} else {
							httponly = true;
						}
					}
					
					if(DOMException.childNodes[y].tagName == "session") {
						if(trim(DOMException.childNodes[y].textContent) == "false") {
							session = false;
						} else {
							session = true;
							
							//WORKAROUND: Firefox does not add session cookies if they have their expiry time set in the past
							expires = new Date().getTime()+10;
						}
					}
					
					if(DOMException.childNodes[y].tagName == "expires") {
						// Extract first integer from text
						expires =
						  parseInt(DOMException.childNodes[y].textContent, 10);
					}
				}
				
				// Make sure the provided information was complete
				if(host    === null || path    === null || name     === null
				|| value   === null || secure  === null || httponly === null
				|| session === null || expires === null) {
					continue;
				}
				
				// Add/modify exception in the permission manager
				mozCookieManager.add(
				  host, path, name, value, secure, httponly, session, expires
				);
			}
		}
		
		return true;
	},
	
	
	/**
	 * Read a cookie storage file and import its settings into the mozilla
	 * cookie manager
	 * 
	 * @param {String|nsIFile|nsIURI|nsIChannel} filepath
	 *        The file location or channel to read
	 * @param {Function} callback=function(){}
	 *        A callback function suppied with the return value of the sync
	 *        function and the status of the input stream
	 * @return {Boolean} Was the aSync call successfully placed?
	 */
	readCookies: function(filepath, callback) {
		// Generate a nsIChannel from the filepath parameter
		var channel = this.newChannelFromURI(filepath);
		if(!channel) {
			return false;
		}
		
		// Place ASync reading call
		var FileManager = this;
		NetUtil.asyncFetch(channel, function(inputStream, status) {
			// Handle all errors
			if(status !== Cr.NS_OK) {
				// Inform callback about the error
				if(callback) {
					callback(false, status);
				}
				
				return;
			}
			
			// Call sync function
			var result = FileManager.readCookiesSync(inputStream);
			// Inform callback about the result
			if(callback) {
				callback(result, status);
			}
		});
		return true;
	},
	
	
	/**
	 * @see readExceptions
	 * 
	 * WARNING: Do not use this function unless you have good reasons, it will
	 *          block the main thread until the whole reading and parsing
	 *          operation is completed
	 * 
	 * @param {String|nsIFile|nsIURI|nsIChannel|nsIInputStream} file
	 *        The filepath or input stream to read
	 * @return {Boolean} Success?
	 */
	readExceptionsSync: function(file) {
		// Mozilla network I/O service
		var mozIOService = Cc["@mozilla.org/network/io-service;1"]
		  .getService(Ci.nsIIOService);
		// Mozilla XML DOM parser service
		var mozXMLDOMParser = Cc["@mozilla.org/xmlextras/domparser;1"]
		  .getService(Ci.nsIDOMParser);
		// Mozilla permission manager service
		var mozPermManager = Cc["@mozilla.org/permissionmanager;1"]
		  .getService(Ci.nsIPermissionManager);
		
		// Generate nsIInputStream from the file parameter
		var stream;
		if(file instanceof Ci.nsIInputStream) {
			// Data input stream (probably called from `readExceptions`)
			stream = file;
		} else {
			// Load the channel from the filepath given
			var channel = this.newChannelFromURI(file);
			if(!channel) {
				return false;
			}
			
			// Open a syncronous stream (BLOCKING!!!)
			stream = channel.open();
		}
		
		// Read data from nsIInputStream
		var data = NetUtil.readInputStreamToString(stream, stream.available());
		
		// Parse XML data into DOM
		var DOM = mozXMLDOMParser.parseFromString(data, "text/xml");
		var DOMDocument = DOM.documentElement;
		
		// Santy check:
		//  Check if the document node is represented by the correct tag
		if(DOMDocument.tagName != "permissions") {
			return false;
		}
		
		for(var x=0; x < DOMDocument.childNodes.length; x++) {
			// Make sure the subnode is an element
			if([1, 9].indexOf(DOMDocument.childNodes[x].nodeType) < 0) {
				continue;
			}
			
			if(DOMDocument.childNodes[x].tagName == "exception") {
				var DOMException = DOMDocument.childNodes[x];
				
				// Initialize host and capability data holders
				var uri        = null;
				var capability = null;
				for(var y = 0; y < DOMException.childNodes.length; y++) {
					// Make sure the subnode is an element
					if([1, 9].indexOf(DOMException.childNodes[y].nodeType) < 0) {
						continue;
					}
					
					if(DOMException.childNodes[y].tagName == "host") {
						var host = DOMException.childNodes[y].textContent;
						// Trim node content
						host = host.replace(/^\s*(\S*(\s+\S+)*)\s*$/, "$1");
						
						if(host == "scheme:file") {
							var uri = mozIOService.newURI("file:///", null, null);
						} else if(host.indexOf("://") > -1) {
							var uri = mozIOService.newURI(host, null, null);
						} else {
							var uri = mozIOService.newURI("http://" + host, null, null);
						}
					}
					
					if(DOMException.childNodes[y].tagName == "capability") {
						// Extract first integer from text
						capability = parseInt(DOMException.childNodes[y].textContent, 10);
					}
				}
				
				// Make sure the information was complete
				if(uri === null || capability === null) {
					continue;
				}
				
				// Add/modify exception in the permission manager
				mozPermManager.add(uri, "cookie", capability);
			}
		}
		
		return true;
	},
	
	
	/**
	 * Read a exception storage file and import its settings into the mozilla
	 * permission manager
	 * 
	 * @param {String|nsIFile|nsIURI|nsIChannel} filepath
	 *        The file location or channel to read
	 * @param {Function} callback=function(){}
	 *        A callback function suppied with the return value of the sync
	 *        function and the status of the input stream
	 * @return {Boolean} Was the aSync call successfully placed?
	 */
	readExceptions: function(filepath, callback) {
		// Generate a nsIChannel from the filepath parameter
		var channel = this.newChannelFromURI(filepath);
		if(!channel) {
			return false;
		}
		
		// Place ASync reading call
		var FileManager = this;
		NetUtil.asyncFetch(channel, function(inputStream, status) {
			// Handle all errors
			if(status !== Cr.NS_OK) {
				// Inform callback about the error
				if(callback) {
					callback(false, status);
				}
				
				return;
			}
			
			// Call sync function
			var result = FileManager.readExceptionsSync(inputStream);
			// Inform callback about the result
			if(callback) {
				callback(result, status);
			}
		});
		return true;
	}
}
