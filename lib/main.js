const sdk = {
	self:        require("sdk/self"),
	simpleprefs: require("sdk/simple-prefs")
};

const tools = {
	dialog: require("./tools/dialog")
};


const csfilemanager = require("./cs-file-manager");

function file_manager_action(options)
{
	var filepath = tools.dialog.fileChooser({
		filename: options.filename,
		filters:  ["xml", "text", "all"],
		mode:     options.mode
	});
	
	if(filepath) {
		var callback = function(is_ok, result)
		{
			if(!is_ok) {
				tools.dialog.message(sdk.self.name, "Error during operation: " + result);
			}
		}
		
		try {
			if(options.method.indexOf("sync") > -1) {
				callback(csfilemanager[options.method](filepath));
			} else {
				csfilemanager[options.method](filepath, callback);
			}
		} catch(e) {
			tools.dialog.exception(e);
		}
	}
}


sdk.simpleprefs.on("btn-cookie-export", function()
{
	file_manager_action({
		filename: "cookies.xml",
		mode:     "save",
		method:   "writeCookiesSync"
	});
});

sdk.simpleprefs.on("btn-cookie-import", function()
{
	file_manager_action({
		filename: "cookies.xml",
		mode:     "open",
		method:   "readCookiesSync"
	});
});

sdk.simpleprefs.on("btn-permission-export", function()
{
	file_manager_action({
		filename: "permissions.xml",
		mode:     "save",
		method:   "writeExceptionsSync"
	});
});

sdk.simpleprefs.on("btn-permission-import", function()
{
	file_manager_action({
		filename: "permissions.xml",
		mode:     "open",
		method:   "readExceptions"
	});
});